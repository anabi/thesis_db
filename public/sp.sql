DROP FUNCTION getTableName(int);
CREATE OR REPLACE FUNCTION getTableName(_vid int)
RETURNS VARCHAR
AS $$
DECLARE tablename varchar(50);
BEGIN
	SELECT "table" INTO tablename FROM masterlut WHERE id = _vid;

	RETURN tablename;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION getvehicles();
CREATE OR REPLACE FUNCTION getVehicles() 
RETURNS TABLE("name" varchar, "id" int, typeid int, "table" varchar)
AS $$
BEGIN
    RETURN QUERY SELECT m."name", m."id",m.typeid, m."table" from masterlut as m;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION getDescriptionTID(int);
CREATE OR REPLACE FUNCTION getDescriptionTID(_tid int)
RETURNS TABLE (columnname varchar, datatype varchar, 
"name" varchar, units varchar)
AS $$
BEGIN
	RETURN QUERY SELECT d.columnname,d.datatype,d."name",d.units 
	FROM description as d
	WHERE d.typeid = _tid
	ORDER BY d.idx ASC;
END
$$ LANGUAGE plpgsql;

DROP FUNCTION getDescription(int);
CREATE OR REPLACE FUNCTION getDescription(_vid int)
RETURNS TABLE (columnname varchar, datatype varchar, 
"name" varchar, units varchar)
AS $$
DECLARE
typeid_ int;
BEGIN
	SELECT typeid INTO typeid_ FROM masterlut WHERE id = _vid;

	RETURN QUERY SELECT d.columnname,d.datatype,d."name",d.units 
	FROM description as d
	WHERE d.typeid = typeid_
	ORDER BY d.idx ASC;
END
$$ LANGUAGE plpgsql;

DROP FUNCTION getTypes();
CREATE OR REPLACE FUNCTION getTypes()
RETURNS TABLE (typeid int, typename varchar)
AS $$
BEGIN
	RETURN QUERY SELECT "id", "type" from types;
END
$$ LANGUAGE plpgsql;

/*
DROP FUNCTION addVehicle(varchar, int);
CREATE OR REPLACE FUNCTION addVehicle (_vname varchar,_typeid int)
AS $$
BEGIN
	-- Get table columns and types from the descriptions table
	
	-- create new table with the same name as the vehicle
	CREATE TABLE 
END
$$ LANGUAGE plpgsql;
*/
DROP FUNCTION getTypeID(VARCHAR);
CREATE OR REPLACE FUNCTION getTypeID(_typename VARCHAR)
RETURNS INT
AS $$
BEGIN
	IF NOT EXISTS (SELECT * FROM types WHERE "type"=_typename) THEN
		INSERT INTO types("type") VALUES (_typename);
	END IF;
	RETURN (SELECT id FROM types WHERE "type"=_typename);
END
$$ LANGUAGE plpgsql;

DROP FUNCTION insertDescription(varchar,int,varchar,varchar,varchar,varchar);
CREATE OR REPLACE FUNCTION insertDescription
(_typename varchar, _idx int, _name varchar,_colname varchar,_units varchar, _datatype varchar)
RETURNS setof void
AS $$
BEGIN

	INSERT INTO description (typeid,idx,name,columnname,units,datatype)
	VALUES ((SELECT getTypeID(_typename)),_idx,_name,_colname,_units,_datatype);


END 
$$ LANGUAGE plpgsql;

DROP FUNCTION getSets(int);
CREATE OR REPLACE FUNCTION getSets(_vid int)
RETURNS TABLE ("start" float, "end" float)
AS $$
BEGIN

	RETURN QUERY SELECT s."start", s."end" FROM Sets as s
	WHERE mid = _vid ORDER BY s."start" DESC;

END
$$ LANGUAGE plpgsql;

DROP FUNCTION validate(varchar(100),varchar(100));
CREATE OR REPLACE FUNCTION validate(_user varchar(100), _pass varchar(100))
RETURNS INT
AS $$
BEGIN
	IF EXISTS(SELECT * FROM Users WHERE username = _user AND "password" = _pass)
	THEN
		RETURN (SELECT 1);
	ELSE
		RETURN (SELECT 0);
	END IF;
END
$$ LANGUAGE plpgsql;



