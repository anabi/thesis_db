DROP TABLE Test CASCADE;
DROP TABLE Types CASCADE;
DROP TABLE MasterLUT CASCADE;
DROP TABLE Description CASCADE;
DROP TABLE JD_1 CASCADE;
DROP TABLE Users CASCADE;

CREATE TABLE Types(
id SERIAL NOT NULL, 
type VARCHAR(50) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE MasterLUT(
id SERIAL NOT NULL,
name VARCHAR(50), 
typeid INT, 
"table" VARCHAR(100),
FOREIGN KEY (typeid) REFERENCES Types(id));

CREATE TABLE Description(
typeid INT,
name VARCHAR(100) NOT NULL,
columnName VARCHAR(100) NOT NULL,
units VARCHAR(10) NOT NULL,
datatype varchar(20) NOT NULL,
idx int NOT NULL,
FOREIGN KEY (typeid) REFERENCES Types(id));


CREATE TABLE JD_1(
northing_r FLOAT,
easting_r FLOAT,
height_r FLOAT,
northing_l FLOAT,
easting_l FLOAT,
height_l FLOAT,
heading FLOAT,
GPS_time FLOAT,
xs_acl_x FLOAT,
xs_acl_y FLOAT,
xs_acl_z FLOAT,
xs_gyro_x FLOAT,
xs_gyro_y FLOAT,
xs_gyro_z FLOAT,
xs_roll FLOAT,
xs_pitch FLOAT,
xs_yaw FLOAT,
xs_time FLOAT,
systime FLOAT,
speed FLOAT,
eng_temp FLOAT,
fuel FLOAT
);

CREATE TABLE Users(
id SERIAL NOT NULL,
username varchar(100) NOT NULL,
password varchar(100) NOT NULL
);

-- populate with data
INSERT INTO Types (type) VALUES ('JD_type');
INSERT INTO MasterLUT(name,typeid,"table") VALUES 
('John Deere 1', 1, 'JD_1'), ('Green Weeder 1', 1, 'GW1_Raw');
INSERT INTO Description(typeid,name,columnName,units,datatype,idx) VALUES
(1,'Northing Right','northing_r','m','float',0), 
(1,'Easting Right','easting_r','m','float',1),
(1,'Height Right','height_r','m','float',2),
(1,'Northing Right','northing_l','m','float',3),
(1,'Easting Right','easting_l','m','float',4),
(1,'Height left','height_l','m','float',5),
(1,'Heading','heading','m','float',6),
(1,'Time','systime',' ','float',7),
(1,'Speed','speed','m/s','float',8),
(1,'Engine Temp','eng_temp','degrees','float',9),
(1,'Fuel','fuel','L','float',10),
(1,'GPS Timestamp','GPS_time','m','float',11),
(1,'Xsens Acceleration X','xs_acl_x','m','float',12),
(1,'Xsens Acceleration Y','xs_acl_y','m','float',13),
(1,'Xsens Acceleration Z','xs_acl_z','m','float',14),
(1,'Xsens Gyro X','xs_gyro_x','m','float',15),
(1,'Xsens Gyro Y','xs_gyro_y','m','float',16),
(1,'Xsens Gyro Z','xs_gyro_z','m','float',17),
(1,'Xsens Roll','xs_roll','m','float',18),
(1,'Xsens Pitch','xs_pitch','m','float',19),
(1,'Xsens Yaw','xs_yaw','m','float',20),
(1,'Xsens Timestamp','xs_time','m','float',21);

-- default admin user
INSERT INTO users(username, "password") VALUES ('admin','unswadmin');
